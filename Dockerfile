FROM python:3-alpine

RUN addgroup -g 1000 -S sigal \
  && adduser -u 1000 -S -G sigal sigal

RUN apk add -u --no-cache ffmpeg \
	&& pip --no-cache-dir install "sigal[all]"

RUN mkdir /sigal /src /html

USER sigal

WORKDIR /sigal

CMD ["sigal", "build", "/src", "/html"]

